import React, { useState, useEffect } from 'react';
import './App.css';


function App() {
  const [state, setState] = useState([]);
  useEffect(() => {
    fetch("http://localhost:3000/scrap", {}).then((response) => response.json())
      .then((data) => {
        console.log(data);
        setState(data);
      })
      .catch((error) => console.log(error));
    ;
  }, []);



  return (
    <div className="App">
      <table>
        <tr>
          <th>Url</th>
          <th>Title</th>
          <th>Headings</th>
          <th>Description</th>
        </tr>
        {state && state.map((el, idx) => {
          if (el.description) {
            return (
              <tr key={idx}>
                <td>{el.mainLink}</td>
                <td>{el.title}</td>
                {el.headings.map((innerEl, newidx) => {
                  return (
                    <tr key={newidx}>
                      <td>{innerEl.tagName + " - " + innerEl.text}</td>
                    </tr>
                  )
                })}
                <td>{el.description}</td>
              </tr>
            )
          }
        })}
      </table>
    </div >
  );
}

export default App;
